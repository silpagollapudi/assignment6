import os
import time


from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application

def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname

def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE Movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year TEXT, title TEXT, director TEXT, actor TEXT, release_date DATE, rating DECIMAL(5,2), PRIMARY KEY (id))'
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
            

def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    entries = []
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cur = cnx.cursor()

        cur.execute("SELECT year, title, director, actor, release_date, rating FROM Movies")
        entries = list(cur.fetchall())
    except Exception as exp:
        print(exp)

    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print("Received request - Highest rated")
    
    db, username, password, hostname = get_db_creds()

    cnx = ''
    entriesPrint = []
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cur = cnx.cursor()

        cur.execute("SELECT year, title, director, actor, rating FROM Movies WHERE rating = ( SELECT MAX(rating) from  Movies )")
        entriesPrint = list(cur.fetchall())
    except Exception as exp:
        messages = "Error getting the highest rated movie"
        print(exp)
        return render_template('index.html', messages=messages)

    return render_template('index.html', entriesPrint=entriesPrint)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print("Received request - Lowest rated")
    
    db, username, password, hostname = get_db_creds()

    cnx = ''
    entriesPrint = []
    messages = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cur = cnx.cursor()

        cur.execute("SELECT year, title, director, actor, rating FROM Movies WHERE rating = ( SELECT MIN(rating) from  Movies )")
        entriesPrint = list(cur.fetchall())
    except Exception as exp:
        messages = "Error getting the lowest rated movie"
        print(exp)
        return render_template('index.html', messages=messages)

    return render_template('index.html', entriesPrint=entriesPrint)


@app.route('/search_movie', methods=['GET'])
def search_movie():
    search_actor = request.args['search_actor']
    print("Received request - Search Actor: %s" % search_actor)
    
    db, username, password, hostname = get_db_creds()

    cnx = ''
    entries = []
    messages = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cur = cnx.cursor()

        cur.execute("SELECT year, title, director, actor, release_date, rating FROM Movies WHERE actor = '" + search_actor + "'")
        entries = list(cur.fetchall())
    except Exception as exp:
        messages = "No movies found for actor " % search_actor
        print(exp)
        return render_template('index.html', messages=messages)

    return render_template('index.html', entries=entries)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received delete request.")
    delete_title = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    messages = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("DELETE FROM Movies WHERE title = '" + delete_title + "'")
        cnx.commit()
        messages = "Movie %s successfully deleted" % delete_title
    except Exception as exp:
        messages = "Movie %s could not be deleted - reason %s" % (delete_title, str(exp))
        print(exp)

    return render_template('index.html', messages=messages)

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received Update request.")
    print(request.form['title'])
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']
    
    db, username, password, hostname = get_db_creds()

    cnx = ''
    messages = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("UPDATE Movies SET year = " + year + ", title = '" + title+ "', director = '" + director + "', actor = '" + actor + "', release_date = '" + release_date + "', rating = " + rating + " WHERE title = '" + title + "'")
        cnx.commit()
        messages = "Movie %s successfully updated" % title
    except Exception as exp:
        messages = "Movie %s could not be updated - reason %s" % (title, str(exp))
        print(exp)

    return render_template('index.html', messages=messages)

@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")
    print(request.form['title'])
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']
    
    db, username, password, hostname = get_db_creds()

    cnx = ''
    messages = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("INSERT INTO Movies (year, title, director, actor, release_date, rating) values (" + year + ",'" + title + "','" + director + "','" + actor + "','" + release_date + "'," + rating + ")")
        cnx.commit()
        messages = "Movie %s successfully inserted" % title
    except Exception as exp:
        messages = "Movie %s could not be inserted - reason %s" % (title, str(exp))
        print(exp)

    return render_template('index.html', messages=messages)


@app.route("/")
def mainpage():
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    return render_template('index.html')


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
